# my-clojure-website

A website written in noir. (1.2.1)

## Install

```bash
lein plugin install lein-noir 1.2.1
```

## Usage

```bash
lein deps
lein run
```

## License

Copyright (C) 2011 FIXME

Distributed under the Eclipse Public License, the same as Clojure.

