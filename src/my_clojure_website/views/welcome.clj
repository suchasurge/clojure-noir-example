(ns my-clojure-website.views.welcome
  (:require [my-clojure-website.views.common :as common]
            [noir.content.getting-started])
  (:use [noir.core :only [defpage]]
        [hiccup.core :only [html]]))

(defpage "/welcome" []
         (common/layout
           [:p "Welcome to my-clojure-website"]))

(defpage "/my-page" []
         (html
          [:h1 "My first page with this framework"]))
